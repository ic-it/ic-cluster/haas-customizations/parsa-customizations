# List all the targets
default:
    just --list

# initialize the project
init:
    #!/usr/bin/env bash

    set -e # exit on error
    set -u # exit on undefined variable
    set -o pipefail # exit on pipe error

    # Check if the virtual environment exists in .venv
    if [ ! -d .venv ]; then
        python3 -m venv .venv
    fi

    # Activate the virtual environment
    source .venv/bin/activate

    # Install the dependencies
    pip install --upgrade pip > /dev/null
    pip install pip-tools > /dev/null
    pip install -r requirements.txt > /dev/null

    # install the ansible dependencies
    ansible-galaxy install -r requirements.yml

    # Deactivate the virtual environment
    deactivate

# run the ansible playbook for debugging purposes
ansible: init
    #!/usr/bin/env bash

    set -e # exit on error
    set -u # exit on undefined variable
    set -o pipefail # exit on pipe error

    # Activate the virtual environment
    source .venv/bin/activate

    # Run the ansible playbook
    ansible-playbook -i inventory/debug.yml main.yml
