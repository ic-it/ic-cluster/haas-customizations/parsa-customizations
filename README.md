# NLP customizations

## Description

This ansible playbook is used on the HaaS service of ic-cluster.
When a user selects on the portal the NLP customizations, this playbook will be run on the machine being setup.

## Usage

```shell
ansible-pull --accept-host-key -U https://gitlab.epfl.ch/ic-it/ic-cluster/haas-customizations/parsa-customizations.git main.yml
```
